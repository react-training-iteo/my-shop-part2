# React Szkolenie - My-shop - Część 2

## Dostępne komendy:

W folderze projektu dostępne są następujące komendy:

**Instalacja zależności (pierwsze uruchomienie)**
### `npm install`

**Start lokalnego serwera w trybie deweloperskim**
### `npm start`

W przeglądarce: [http://localhost:3000](http://localhost:3000) adres lokalnego serwera.

**Uruchamianie testów**
### `npm test`

**Tworzenie wersji produkcyjnej**
### `npm run build`
gotowy produkt znajduje się w folderze: build


**Dodanie plików konfiguracyjnych do folderu projektu**
#### `npm run eject`
**WAŻNE: użycie operacji EJECT nie pozwala na przywrócenie poprzedniego stanu!**

