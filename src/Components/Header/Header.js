import React from 'react';
import './header.css';
import logo from './../../assets/example-logo.png';

class Header extends React.Component {

  render() {
    return (
      <header className="header-app"><img className="header-app--logo" src={logo} alt="Logo" /> My-shop application</header>
      )
  }
}

export default Header;
