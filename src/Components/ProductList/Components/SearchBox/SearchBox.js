import React from 'react';
import './SearchBox.css';

class SearchBox extends React.Component {
  render() {
    return (
      <div className="search">
        <input className="search-input" placeholder="Szukaj..."/>
      </div>
      )
  }
}

export default SearchBox;
