import React from 'react';
import './Product.css';

class Product extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {name, description, price, stock} = this.props;
    return (
      <div className="product-box">
        <div>
          <img className="product-box--image" src="http://lorempixel.com/140/180/" />
        </div>
        <div>
          <h2>{name}</h2>
          <pre>Cena: {`${price}zł`}</pre>
          <p>{description}</p>
          <button disabled={!stock}>Dodaj do koszyka</button>
        </div>
      </div>
    )
  }
}

export default Product;
