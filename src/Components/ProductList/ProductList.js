import React from 'react';
import './ProductList.css';
import SearchBox from './Components/SearchBox';
import Product from './Components/Product'

class ProductList extends React.Component {

  getProducts() {
    const {productList} = this.props;
    return productList.map((value, index) => (
      <Product key={index} name={value.name} description={value.description} price={value.price} stock={value.in_stock} />
    ))
  }

  render() {
    return (
      <section className="product-list">
        <SearchBox/>
        {this.getProducts()}
      </section>
      )
  }
}

export default ProductList;
