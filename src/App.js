import React, { Component } from 'react';

import './App.css';
import Header from './Components/Header';
import Footer from './Components/Footer';
import ProductList from './Components/ProductList';
import Cart from './Components/Cart';

class App extends Component {
  constructor() {
    super();

    this.state = {
      productList: [
        {id: Math.random(), name: 'Sony Play Station 4', description: 'Poznaj bardziej smukłe, mniejsze PS4, które oferuje graczom niesamowite wrażenia z gier.', price: 1399, in_stock: true},
        {id: Math.random(), name: 'Xbox One X', description: 'Na Xbox One X gra się jeszcze lepiej. Dzięki większej o 40% mocy niż na jakiejkolwiek innej konsoli.', price: 1500, in_stock: true},
        {id: Math.random(), name: 'Macbook PRO 2018', description: 'MacBook Pro - dotyk czyni cuda Jest niewiarygodnie smukły, lekki jak piórko, a do tego potężniejszy i szybszy niż kiedykolwiek', price: 14900, in_stock: true},
        {id: Math.random(), name: 'Macbook Air 2018', description: '11-calowy MacBook Air działa bez ładowania baterii do 9 godzin, a 13-calowy - nawet 12.', price: 3600, in_stock: true},
        {id: Math.random(), name: 'iPhone X', description: 'iPhone X to telefon, który nie tyle posiada ekran, ile po prostu sam tym ekranem jest.', price: 7299, in_stock: false},
      ]
    };
  }
  render() {
    return (
      <div className="App">
        <Header />
        <div className="container">
          <ProductList {...this.state}/>
          <Cart/>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
